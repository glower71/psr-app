<?php

return [
    'providers' => [
        \Framework\Http\RouteProvider::class,
        \Framework\Database\DatabaseProvider::class,
    ],
    'routes' => [
        [
            'pattern' => '/',
            'handler' => \App\Controllers\HomeController::class
        ],
        [
            'pattern' => '/api/book_author/(?P<id>\d+)',
            'handler' => \App\Controllers\Api\BookAuthor::class
        ],
        [
            'pattern' => '/api/author_books/(?P<id>\d+)',
            'handler' => \App\Controllers\Api\AuthorBooks::class
        ],
        [
            'pattern' => '/api/book_3_authors',
            'handler' => \App\Controllers\Api\Book3Authors::class
        ],
    ],
    'database' => [
        'connection' => [
            'host' => 'localhost',
            'dbname' => 'psr_app',
            'username' => 'psr_app',
            'passwd' => 'Psr_app123',
        ]
    ],
];