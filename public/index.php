<?php

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

$app = new \Framework\Application();

$request = new \Framework\Http\ServerRequest();
$response = $app->run($request);

$emitter = new \Framework\Http\Emitter();
$emitter->emit($response);
