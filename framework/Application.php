<?php

namespace Framework;

use Framework\Container\Container;
use Psr\Http\Message\RequestInterface;

class Application extends Container
{
    public function __construct()
    {
        $this->boot();
    }

    protected function boot()
    {
        $config = include __DIR__ . '/../config/app.php';
        $this->set('config', $config);

        foreach ($config['providers'] as $provider) {
            /**
             * @var Provider $item
             */
            $item = new $provider($this);
            $item->boot();
        }
    }

    public function run(RequestInterface $request)
    {
        $result = $this->get('router')->match($request);

        $handler = $result->getHandler();
        $attributes = $result->getAttributes();

        foreach ($attributes as $name => $value) {
            $request = $request->withAttribute($name, $value);
        }

        $response = $handler::handle($request);
        return $response;
    }
}