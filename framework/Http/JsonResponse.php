<?php

namespace Framework\Http;

class JsonResponse extends Response
{
    public function __construct($body, $status = 200)
    {
        if (is_array($body)) {
            $body = json_encode($body);
        };
        parent::__construct($body, $status);
        $this->headers['Content-Type'] = 'application/json';
    }
}