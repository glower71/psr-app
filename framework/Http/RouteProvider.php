<?php

namespace Framework\Http;

use Framework\Provider;

class RouteProvider extends Provider
{
    public function boot()
    {
        $routes = new \Framework\Http\Router\RouteCollection();

        $configRoutes = $this->app->get('config')['routes'];
        foreach ($configRoutes as $configRoute) {
            $routes->add($configRoute['pattern'], $configRoute['handler']);
        }

        $router = new \Framework\Http\Router\Router($routes);
        $this->app->set('router', $router);
    }
}