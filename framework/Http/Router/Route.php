<?php

namespace Framework\Http\Router;

use Framework\Http\Router\Result;
use Psr\Http\Message\ServerRequestInterface;

class Route
{
    private $pattern;
    private $handler;

    public function __construct($pattern, $handler)
    {
        $this->pattern = $pattern;
        $this->handler = $handler;
    }

    public function match(ServerRequestInterface $request): ?Result
    {
        $path = $request->getUri()->getPath();
        if (!preg_match('~^' . $this->pattern . '$~i', $path, $matches)) {
            return null;
        }

        return new Result(
            $this->handler,
            array_filter($matches, '\is_string', ARRAY_FILTER_USE_KEY)
        );
    }
}
