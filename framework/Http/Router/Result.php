<?php

namespace Framework\Http\Router;

class Result
{
    private $handler;
    private $attributes;

    public function __construct($handler, array $attributes)
    {
        $this->handler = $handler;
        $this->attributes = $attributes;
    }

    /**
     * @return mixed
     */
    public function getHandler()
    {
        return $this->handler;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }
}
