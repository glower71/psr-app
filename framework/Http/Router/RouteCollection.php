<?php

namespace Framework\Http\Router;

use Framework\Http\Router;

class RouteCollection
{
    private $routes = [];

    public function addRoute(Route $route): void
    {
        $this->routes[] = $route;
    }

    public function add($pattern, $handler): void
    {
        $this->addRoute(new Route($pattern, $handler));
    }

    /**
     * @return Route[]
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }
}
