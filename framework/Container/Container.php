<?php

namespace Framework\Container;

use Psr\Container\ContainerInterface;

class Container implements ContainerInterface
{
    protected $definitions = [];
    protected $results = [];

    /**
     * @inheritDoc
     */
    public function get($id)
    {
        if (array_key_exists($id, $this->results)) {
            return $this->results[$id];
        }

        if (!array_key_exists($id, $this->definitions)) {
            throw new NotFoundException();
        }

        $definition = $this->definitions[$id];

        if ($definition instanceof \Closure) {
            $this->results[$id] = $definition();
        } else {
            $this->results[$id] = $definition;
        }

        return $this->results[$id];
    }

    /**
     * @inheritDoc
     */
    public function has($id)
    {
        if (isset($this->definitions[$id])) {
            return true;
        }
    }

    public function set($id, $value): void
    {
        if (array_key_exists($id, $this->results)) {
            unset($this->results[$id]);
        }

        $this->definitions[$id] = $value;
    }
}