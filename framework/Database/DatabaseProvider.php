<?php

namespace Framework\Database;

use Framework\Application;
use Framework\Provider;

class DatabaseProvider extends Provider
{
    /**
     * @var Application
     */
    protected $app;
    protected $connection;

    public function boot()
    {
        $config = $this->app->get('config')['database']['connection'];
        Query::$connection = new \mysqli($config['host'], $config['username'], $config['passwd'], $config['dbname']);
    }
}