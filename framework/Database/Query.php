<?php

namespace Framework\Database;

use mysqli;

class Query
{
    /**
     * @var mysqli
     */
    public static $connection;

    public static function query($query)
    {
        /**
         * ToDO SQL-injections defence :)
         */
        return static::$connection->query($query);
    }
}