<?php

namespace App\Controllers\Api;

use Framework\Database\Query;
use Framework\Http\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class AuthorBooks implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $res = [];
        $id = $request->getAttribute('id');
        $result = Query::query('select books.* from book_authors join books on books.id = book_authors.book_id where book_authors.author_id = "' . $id . '"');
        while($resultArray = mysqli_fetch_assoc($result)) {
            $res[] = $resultArray;
        }

        return new JsonResponse($res, 200);
    }
}
