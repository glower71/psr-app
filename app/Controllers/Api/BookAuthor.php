<?php

namespace App\Controllers\Api;

use Framework\Database\Query;
use Framework\Http\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class BookAuthor implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $res = [];
        $id = $request->getAttribute('id');
        $result = Query::query('select authors.* from authors right join book_authors on authors.id = book_authors.id where book_authors.book_id = "' . $id . '"');
        while($resultArray = mysqli_fetch_assoc($result)) {
            $res[] = $resultArray;
        }
        return new JsonResponse($res, 200);
    }
}
