<?php

namespace App\Controllers\Api;

use Framework\Database\Query;
use Framework\Http\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Book3Authors implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $res = [];
        $result = Query::query('select books.*, count(book_authors.author_id) as count_authors from book_authors left join books on book_authors.book_id = books.id group by book_authors.book_id having count(book_authors.author_id) = 3;');
        while($resultArray = mysqli_fetch_assoc($result)) {
            $res[] = $resultArray;
        }

        return new JsonResponse($res, 200);
    }
}
