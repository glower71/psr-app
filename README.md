������������ ������ ������� ������� ������ Application, ���������� ������������ ����������� Psr\ContainerInterface;

��� �������� $app ���������� �������� ��������� �����������, ������������ � ����� config/app.php.

��� ���������� (Router, Database) ��������� � ��������� $app ���� ���������. ������ � ArrayAccess ����������� �� ���� �� ������� �������� �������.

� ����� public/index.php ��������� ������ $app, ����������� �������� ��� �����������. ������ ������ ����� � ����� run, �������� ���������� Psr\ServerRequestInterface. ������ �����������, �� ��������������� ���������� �� method. �������� ����� ����� ��� ��������� - ���������� ��������� � ������� RequestHandlerInterface. ���� ����������� ����� � ����� app, ������� ��������� ������� ��� MVC. ��������� �������� ���, ����� View ��������� MVC �����������.

������� � �� ����� � ��������� ����� Query. ������ �� sql-�������� ������ �� ����. �������� �� ������� ORM, �� ������� �������� �� �������.

����� �������� � ���� Psr\ResponseInterface.

������ �� ����� ��������� �������-���������. ������� � ����� ��������� ���������, ����� ������ ���� ���� ������.

��������� ������ � ����� ������ �� ���� �� ������� �������� �������. Middleware ��� ��������� ������ �����������.

�������������� PSR-����������:
- Container 
- ServerRequest
- Response
- RequestHandlerInterface

�������� �������� �������, �� ����������� �� ���� �� ��������� ���������. � composer.json ���������� ������ PSR-����������.

���� �����-���������� ����� � framework, ����� app � ������ ������ �������� �����������. ������������ �������� � /config/app.php. ������ �� �� ����� � ������� ��� git. ����������� .env ��� � Laravel �� ����.

books:  
id, name  
1	��� ��� ��������  
2	����������� ��������  
3	��������� ����������  
4	ERP ��� ����� ���������  
5	���� � ����  

authors:  
id, name  
1	������  
2	������  
3	�������  
4	��������  
5	�����  
6	�������  
7	������  

book_authors:
id, book_id, author_id  
1	1	2  
2	2	3  
3	2	4  
4	3	1  
6	3	2  
5	3	5  
7	4	1  
8	4	7  
9	5	1  
10	5	4  
11	5	7  
13	6	1  
12	6	2

